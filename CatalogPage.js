import { getProducts } from './API.js';
import { generateProduct } from './Product.js';

const catalog = document.getElementsByClassName('main__item--catalog')[0];

const generateProductsHTML = (productList) => {
    let productHTML = '';

    for(let product of productList) {
        productHTML += generateProduct(product);
    }

    return productHTML;
}

const fillCatalog = async (sort = null) => {
    const productList = await getProducts(sort);
    catalog.innerHTML = generateProductsHTML(productList);
}

fillCatalog();


//SORT

const sort = document.getElementById('sort');
sort.addEventListener('change', () => {
    // console.log(sort.value);
    fillCatalog(sort.value);
});

//SEARCH

const searchQuery = document.getElementById('searchQuery');
const searchButton = document.getElementById('searchButton');
searchButton.addEventListener('click', () => {
    getProducts(sort).then(result => {
        const filteredProductsList = result.filter(item => item.title.toLowerCase().indexOf(searchQuery.value.toLowerCase()) !== -1);
        catalog.innerHTML = generateProductsHTML(filteredProductsList);
    })
});