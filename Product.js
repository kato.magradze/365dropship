
export const generateProduct = (product) => 
        `<div class="catalog__item catalog__item--product">
        <div class="product__item product__item--image">
            <img class="product__img" src="${product.image}"/>
        </div>
        <div class="product__item product__item--title">
            <h3 class="product__heading">${product.title}</h3>
        </div>
        <div class="product__item product__item--price">
            <div class="price__item">RRP: ${product.price}$</div>
            <div class="price__item">Profit: 25% / $2</div>
            <div class="price__item">Cost: $4</div>
        </div>
        </div>
        `;