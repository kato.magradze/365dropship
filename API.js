import { SERVER_ADDRESS } from "./Config.js" 

const request = async (query) => {
    try {
        const req = await fetch(SERVER_ADDRESS + query);
        const res = await req.json();
        return res;
    }
    catch(err) {
        console.log(err);
    }
}

export const getProducts = async (sort = null) => {
    return await request(`products${sort ? `?sort=${sort}` : ''}`);
}

export const getCategories = async () => await request('products/categories');

